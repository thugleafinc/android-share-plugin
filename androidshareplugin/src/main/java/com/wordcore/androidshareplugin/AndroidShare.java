package com.wordcore.androidshareplugin;

import android.app.Activity;
import android.app.Fragment;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.unity3d.player.UnityPlayer;

import java.io.File;

/**
 * Created by Yassine on 04/06/2016.
 */
public class AndroidShare extends Fragment {

    public static final String TAG = AndroidShare.class.getSimpleName();
    public static final int SHARE_TEXT_REQUEST = 1003;
    public static final int SHARE_IMAGE_TEXT_REQUEST = 1004;
    private static final String CALLBACK_METHOD_NAME = "IntentFinishedWithResult";

    public static AndroidShare instance;
    private String gameObjectName;
    private static Activity unityActivity;
    private Uri uri;

    public static void start(String gameObjectName)
    {
        unityActivity = UnityPlayer.currentActivity;
        instance = new AndroidShare();
        instance.gameObjectName = gameObjectName;
        unityActivity.getFragmentManager().beginTransaction().add(instance, AndroidShare.TAG).commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void shareGameResultAndScreenshot(String filePath, String resultMessage)
    {
        Log.d(TAG,"shareGameResultAndScreenshot from "+filePath);
        try{
            uri = Uri.fromFile(new File(filePath));

            Log.d(TAG, uri.toString());

            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra(android.content.Intent.EXTRA_TEXT, resultMessage);
            intent.putExtra(Intent.EXTRA_STREAM, getImageContentUri(unityActivity.getBaseContext(), filePath));
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent,"Share your result"), SHARE_IMAGE_TEXT_REQUEST);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(unityActivity.getBaseContext(),"Error while sharing result: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void shareMessage(String chooserTitle, String message)
    {
        try{
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, message);
            startActivity(Intent.createChooser(intent, chooserTitle));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(unityActivity.getBaseContext(),"Error while sharing message: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode) {
            case SHARE_IMAGE_TEXT_REQUEST:
                if(resultCode == Activity.RESULT_OK){
                    Log.d(TAG, "Share text and image onActivityResult RESULT_OK");
                    UnityPlayer.UnitySendMessage(gameObjectName, CALLBACK_METHOD_NAME, null);
                } else
                    Log.d(TAG, "Share text and image onActivityResult RESULT_NOT_OK");
                deleteScreenShot();
                break;
        }
    }

    private void deleteScreenShot(){
        try {
            File file = new File(uri.getPath());
            if(file.exists())
                file.delete();
            else
                Log.d(TAG, "File does not exist.");
        }
        catch (Exception e){
            Log.d(TAG, "Exception while deleting file " + e.getMessage());
        }
    }

    public static Uri getImageContentUri(Context context, String filePath) {
        File imageFile = new File(filePath);
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Images.Media._ID },
                MediaStore.Images.Media.DATA + "=? ",
                new String[] { filePath }, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }


}
